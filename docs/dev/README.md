# UpdateGuestService Developer Documentation

## Technologies

- Database: [MongoDB](https://www.mongodb.com/)
- Server: [Node.js](https://nodejs.org/en/)

## API Endpoints

|Type|Endpoint|Query|Body|Return|
|---|---|---|---|---|
|GET|api/guestUpdates|None|{<br>"startDate":DATE\*,<br>"endDate":DATE\*<br>}|Array of all updated records in date range.<br>RANGE: From greater than or equal to startDate, to less than endDate.
|GET|api/guestUpdates/updates|None|None|Array of all updated records.
|GET|api/guestUpdates/{id}|None|None|Array of updates for requested ID
|POST|api/guestUpdates|None|*Optional* **Required**<br>{<br>"**id**":String,<br>"*registrationDate*":String,<br>"*ageRange*":Number,<br>"*isResident*":Boolean,<br>"*zipCode*":String,<br>"*socialSecurity*":Boolean,<br>"*tanfeadc*":Boolean,<br>"*snap*":Boolean,<br>"*wic*":Boolean,<br>"*sfsp*":Boolean,<br>"*schoolBreakfast*":Boolean,<br>"*schoolLunch*":Boolean,<br>"*financialAid*":Boolean,<br>"*otherBenefit*":String,<br>"*employed*":Boolean,<br>"*householdSize*":Number<br>}|Newly created update record<br>{<br>"_id":ObjectId,<br>"**id**":String,<br>"*registrationDate*":String,<br>"*ageRange*":Number,<br>"*isResident*":Boolean,<br>"*zipCode*":String,<br>"*socialSecurity*":Boolean,<br>"*tanfeadc*":Boolean,<br>"*snap*":Boolean,<br>"*wic*":Boolean,<br>"*sfsp*":Boolean,<br>"*schoolBreakfast*":Boolean,<br>"*schoolLunch*":Boolean,<br>"*financialAid*":Boolean,<br>"*otherBenefit*":String,<br>"*employed*":Boolean,<br>"*householdSize*":Number,<br>"modificationDate":Date<br>}|
|DELETE|api/guestUpdates/{id}|None|None|Stubbed message 'DELETE'|

\* DATE format: YYYY-MM-DD

## Documentation

- [Browse on librefoodpantry.org](https://librefoodpantry.org/#/projects/StandardProject/)
- [Edit on GitLab](docs) (in `docs/` directory)


---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.

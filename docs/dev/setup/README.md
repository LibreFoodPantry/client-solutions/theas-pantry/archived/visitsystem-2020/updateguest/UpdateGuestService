# UpdateGuestService Developer Setup

## Setup Instructions

- This service requires [MongoDB](https://www.mongodb.com/) to be installed and running.
- [Node.js and Node Package Manager required.](https://nodejs.org/en/)
- All following commands shuld be run from the root directory, the directory with package.json.
- From local repository, run ``npm install`` in command line to install dependencies.
- To start backend server run ``node index`` in command line.
    - If editing, run ``nodemon index`` instead to automatically restart server.

#### Docker deployment
- [Docker](https://www.docker.com/) installed and running.
- From local repository, run 'docker-compose up' to run service in docker.

## Documentation

- [Browse on librefoodpantry.org](https://librefoodpantry.org/#/projects/StandardProject/)
- [Edit on GitLab](docs) (in `docs/` directory)


---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.

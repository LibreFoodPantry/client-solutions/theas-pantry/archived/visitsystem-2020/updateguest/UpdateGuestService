# UpdateGuestService Developer Testing

## Testing
- Run command ``npm test`` from repository to run mocha tests.
- Run command ``npm run coverage`` from repository to run code coverage with tests.
    - Results will be stored in coverage directory.
- Run command ``npm run api:test`` to test api endpoints.
    - **This process does not auto-terminate after tests. Use ctrl+c to terminate.**
    - The database collection created for these tests is discarded at process termination.
    - If desired, manual testing can be done on test server before terminating process.

API tests were written using [Postman](https://www.postman.com/). The request collection can be imported into Postman by using the .postman_collection file in ``/test/API``
- The tests can be run individually by sending the requests manually. The test results will appear in the Test Results tab in the response area. <br><br>
    ![alt text](endpoint_test_results.png "Request test results")

- To run the tests as a collection, select the postman collection and select run. <br><br>
    ![alt text](run_collection.png "Run Postman collection")

- Tests can be written using the Tests tab. <br><br>
    ![alt text](endpoint_testing.png "Request test results")


## Testing Tools
- [Mocha](https://mochajs.org/) for testing
- [istanbul](https://istanbul.js.org/) and it's [CLI nyc](https://github.com/istanbuljs/nyc)

## Documentation

- [Browse on librefoodpantry.org](https://librefoodpantry.org/#/projects/StandardProject/)
- [Edit on GitLab](docs) (in `docs/` directory)


---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.

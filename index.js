const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
app.use(cors());

mongoose.connect('mongodb://mongodb:27017/guestUpdates', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
mongoose.Promise = global.Promise;

app.use(bodyParser.json());
app.use('/api', require('./routes/api'));

app.use(function (err, req, res, next) {
    if (err.name === 'CastError') {
        res.status(404).send({ CastError: err.message });
    }
    else if (err.name === 'ValidationError') {
        res.status(400).send({ ValidationError: err.message });
    }
    else {
        res.status(500).send({ error: err });
    }
});

app.listen(3000, function () {
    console.log('now listening for requests');
});

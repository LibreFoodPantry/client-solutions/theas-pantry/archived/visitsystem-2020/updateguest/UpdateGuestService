const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GuestSchema = new Schema({
    id: {
        type: String,
        required: [true, 'Guest ID field is required']
    },
    registrationDate: String,
    ageRange: Number,
    isResident: Boolean,
    zipCode: String,
    socialSecurity: Boolean,
    tanfeadc: Boolean,
    snap: Boolean,
    wic: Boolean,
    sfsp: Boolean,
    schoolBreakfast: Boolean,
    schoolLunch: Boolean,
    financialAid: Boolean,
    otherBenefit: String,
    employed: Boolean,
    householdSize: Number
}, {
    timestamps: { createdAt: 'modificationDate', updatedAt: false }
});

const Guest = mongoose.model('guest', GuestSchema);

module.exports = Guest;

const express = require('express');
const router = express.Router();
const Guest = require('../models/guest');

router.get('/guestUpdates', function(req, res, next) {
    Guest.find({
        modificationDate: { $gte: req.body.startDate, $lt: req.body.endDate }
    }).then(function(guests) {
        res.send(guests);
    }).catch(next);
});

router.post('/guestUpdates', function(req, res, next) {
    Guest.create(req.body).then(function(guest) {
        res.status(201);
        res.send(guest);
    }).catch(next);
});

router.delete('/guestUpdates/:id', function(req, res) {
    res.send({ type: 'DELETE' });
});

router.get('/guestUpdates/updates', function(req, res, next) {
    Guest.find({}).then(function(updates) {
        res.send(updates);
    }).catch(next);
});

router.get('/guestUpdates/:id', function(req, res, next) {
    Guest.find({ id: req.params.id }).then(function(updates) {
        res.send(updates);
    }).catch(next);
});

module.exports = router;

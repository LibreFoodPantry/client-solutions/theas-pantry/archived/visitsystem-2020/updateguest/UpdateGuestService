const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();

mongoose.connect('mongodb://localhost/testApi', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

mongoose.Promise = global.Promise;

app.use(bodyParser.json());
app.use('/api', require('../../routes/api'));

app.use(function(err, req, res, next) {
    if (err.name === 'CastError') {
        res.status(404).send({ CastError: err.message });
    }
    else if (err.name === 'ValidationError') {
        res.status(400).send({ ValidationError: err.message });
    }
    else {
        res.status(500).send({ error: err });
    }
});

app.listen(3000, function() {
    console.log('now listening for requests');
});

// catching signals and do something before exit
['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
    'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
].forEach(function(sig) {
    process.on(sig, function() {
        exitTasks(sig);
    });
});

function exitTasks(sig) {
    if (typeof sig === "string") {
        mongoose.connection.db.dropDatabase(function() {
            process.exit(1);
        });
    }
}

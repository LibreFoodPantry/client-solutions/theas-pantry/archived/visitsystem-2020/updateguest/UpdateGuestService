const assert = require('assert');
const guest = require('../models/guest');
const exampleGuestsData = require("./example_guests");

describe('Changing guest record test', function() {
    let exampleGuests;
    before(function(done) {
        exampleGuests = [
            new guest(exampleGuestsData.ONE),
            new guest(exampleGuestsData.TWO),
            new guest(exampleGuestsData.THREE),
            new guest(exampleGuestsData.FOUR),
            new guest(exampleGuestsData.FIVE)
        ];
        guest.insertMany(exampleGuests, function(err) {
            if (err) {
                return console.error(err);
            } else {
                console.log('Docs inserted');
            }
            done();
        });
    });

    it('Update one record\'s ID', function(done) {
        guest.findOneAndUpdate({ id: exampleGuests[3].id }, { id: '0000' }, { useFindAndModify: false }).then(function() {
            guest.findOne({ _id: exampleGuests[3]._id }).then(function(result) {
                assert(result.id === '0000');
                done();
            });
        });
    });
});

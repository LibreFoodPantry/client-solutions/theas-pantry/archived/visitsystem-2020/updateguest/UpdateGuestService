const mongoose = require('mongoose');

before(function(done) {
    mongoose.connect('mongodb://localhost/testGuestSchema', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    mongoose.connection.once('open', function() {
        console.log('Connection Successful');
    }).on('error', function(error) {
        console.log('Connection error: ', error);
    });
    done();
});

afterEach(function(done) {
    mongoose.connection.db.dropCollection('guests', function() {
        done();
    });
});

after(function(done) {
    mongoose.connection.db.dropDatabase(function() {
        mongoose.disconnect(function() {
            done();
        });
    });
});

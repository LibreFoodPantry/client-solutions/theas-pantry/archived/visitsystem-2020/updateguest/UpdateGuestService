const assert = require('assert');
const guest = require('../models/guest');
const exampleGuestsData = require('./example_guests');

describe('Deleting guests from database', function() {
    let exampleGuests;
    before(function(done) {
        exampleGuests = [
            new guest(exampleGuestsData.ONE),
            new guest(exampleGuestsData.TWO),
            new guest(exampleGuestsData.THREE),
            new guest(exampleGuestsData.FOUR),
            new guest(exampleGuestsData.FIVE)
        ];
        guest.insertMany(exampleGuests, function(err) {
            if (err) {
                return console.error(err);
            } else {
                console.log('Docs inserted');
            }
            done();
        });
    });
    it('Deleting record test', function(done) {
        let currentLength;
        guest.find({}).then(function(result) {
            currentLength = result.length;
        });
        guest.findOneAndDelete({ _id: exampleGuests[3]._id }).then(function() {
            guest.find({}).then(function(result) {
                assert(result.length === currentLength - 1);
            });
        });
        guest.find({ _id: exampleGuests[3]._id }).then(function(result) {
            assert(result.length === 0);
            done();
        })
    });
});

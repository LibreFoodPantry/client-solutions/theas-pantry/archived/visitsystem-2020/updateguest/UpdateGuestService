const assert = require('assert');
const guest = require('../models/guest');
const exampleGuestsData = require("./example_guests");

describe('Saving new update to database', function() {

    let exampleGuests;
    before(function(done) {
        exampleGuests = [
            new guest(exampleGuestsData.ONE),
            new guest(exampleGuestsData.TWO),
            new guest(exampleGuestsData.THREE),
            new guest(exampleGuestsData.FOUR),
            new guest(exampleGuestsData.FIVE)
        ];
        guest.insertMany(exampleGuests, function(err) {
            if (err) {
                return console.error(err);
            } else {
                console.log('Docs inserted');
            }
            done();
        });
    });

    it('Check new record', function(done) {
        guest.findOne({ _id: exampleGuests[0]._id }).then(function(result) {
            assert(result.id === exampleGuests[0].id);
            assert(result.ageRange === exampleGuests[0].ageRange);
            assert(result.isResident === exampleGuests[0].isResident);
            assert(result.zipCode === exampleGuests[0].zipCode);
            assert(result.socialSecurity === exampleGuests[0].socialSecurity);
            assert(result.tanfeadc === exampleGuests[0].tanfeadc);
            assert(result.snap === exampleGuests[0].snap);
            assert(result.wic === exampleGuests[0].wic);
            assert(result.sfsp === exampleGuests[0].sfsp);
            assert(result.schoolBreakfast === exampleGuests[0].schoolBreakfast);
            assert(result.schoolLunch === exampleGuests[0].schoolLunch);
            assert(result.financialAid === exampleGuests[0].financialAid);
            assert(result.otherBenefit === exampleGuests[0].otherBenefit);
            assert(result.employed === exampleGuests[0].employed);
            assert(result.householdSize === exampleGuests[0].householdSize);
            assert(result.modificationDate.toString() === exampleGuests[0].modificationDate.toString());
            done();
        });
    });

    it('Check number of entries', function(done) {
        let currentLength;
        guest.find({}).then(function(result) {
            currentLength = result.length;
        });
        let nextUpdate = new guest(exampleGuestsData.TWO);
        nextUpdate.save().then(function() {
            guest.find({}).then(function(result) {
                assert(result.length === currentLength + 1);
                done();
            });
        });
    });

    it('Checking for required ID Validation error', function(done) {
        let badGuest = new guest({ registrationDate: '01-01-0001' });
        let badSave = async function() {
            try {
                await badGuest.save();
            } catch (err) {
                assert(err.name === 'ValidationError');
            }
        };
        badSave();
        done();
    });
});
